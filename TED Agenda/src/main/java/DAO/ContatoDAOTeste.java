package DAO;

import java.util.ArrayList;
import java.util.Scanner;

import domain.Contatos;
import domain.Endereco;
import DAO.ContatoDAO;

public class ContatoDAOTeste {

	private static ArrayList<String> telefones;
	private static Endereco endereco;

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		int opcao;

		System.out.println("Seja bem vindo a sua agenda!");

		System.out.println("Para inserir um contato digite 1");
		System.out.println("Para alterar um contato digite 2");
		System.out.println("Para consultar um contato digite 3");
		System.out.println("Para remover um contato digite 4");

		opcao = sc.nextInt();

		switch (opcao) {
		case 1:
			inserirContato();

			break;
		case 2:
			alterarContato();
			break;
		case 3:
			consultarContato();
			break;
		case 4:
			removerContato();
			break;
		}

	}

	public static void inserirContato() throws Exception {

		Contatos contato = new Contatos();
		contato.setNome("Murilo Guerreiro");
		contato.setTelefones(telefones);
		contato.setEndereco(endereco);

		ContatoDAO dao = new ContatoDAO();
		System.out.println("Salvando o contato: " + contato.getNome());
		contato = dao.salvar(contato);
	}

	public static void alterarContato() throws Exception {
		Contatos contato = new Contatos();
		contato.setNome("Antonio Claudio");
		ContatoDAO dao = new ContatoDAO();
		contato = dao.salvar(contato);
		System.out.println("Alterando o contato: " + contato.getNome());
	}

	public static void consultarContato() {
		Contatos contato = new Contatos();
		ContatoDAO dao = new ContatoDAO();
		Contatos contato1 = dao.consultarPorId(contato.getId());
		System.out.println("Consultando: " + contato1.getNome());

	}

	public static void removerContato() {

		Contatos contato = new Contatos();
		ContatoDAO dao = new ContatoDAO();
		System.out.println("Removendo a pessoa: " + contato.getId());
		dao.excluir(contato.getId());

	}

}
