package domain;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import converter.Converter;
import enums.Tipo;

@Embeddable
public class Endereco {

	@Column(length = 40, nullable = false)
	private String logradouro;

	@Column(length = 40, nullable = false)
	private String bairro;

	@ManyToOne
	private Cidade cidade;

	@Convert(converter = Converter.class)
	@Column(columnDefinition = "char(3)", nullable = false)
	private Tipo tipo;
	
	public Endereco() {
	}
	
	public Endereco(String logradouro, String bairro, Cidade cidade) {
		super();
		this.logradouro = logradouro;
		this.bairro = bairro;
		this.cidade = cidade;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	@Override
	public String toString() {
		return "Endereco [logradouro=" + logradouro + ", bairro=" + bairro + ", cidade=" + cidade + "]";
	}

}
