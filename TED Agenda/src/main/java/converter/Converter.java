package converter;

import javax.persistence.AttributeConverter;

import enums.Tipo;;

public class Converter implements AttributeConverter<Tipo, String> {

	public String convertToDatabaseColumn(Tipo tipo) {
		return tipo != null ? tipo.getCodigo() : null;
	}

	public Tipo convertToEntityAttribute(String dbData) {
		return dbData != null ? Tipo.valueOfCodigo(dbData) : null;
	}

}
