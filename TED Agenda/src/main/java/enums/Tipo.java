package enums;

public enum Tipo {

	COMERCIAL("CMC"), RESIDENCIAL("RSD");

	private String codigo;
	
	private Tipo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}
	
	public static Tipo valueOfCodigo(String codigo) {
		for (Tipo tipo : values()) {
			if (tipo.getCodigo().equalsIgnoreCase(codigo)) {
				return tipo;
			}
		}
		throw new IllegalArgumentException("Codigo nao valido: " + codigo);
	}
}
