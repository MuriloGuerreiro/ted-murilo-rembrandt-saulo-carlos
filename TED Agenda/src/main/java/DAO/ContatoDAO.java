package DAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import domain.Contatos;

public class ContatoDAO {
	
		  private EntityManager getEntityManager() {
		    EntityManagerFactory factory = null;
		    EntityManager entityManager = null;
		    try {
		      //Obt�m o factory a partir da unidade de persist�ncia.
		      factory = Persistence.createEntityManagerFactory("Project-JPA");
		      //Cria um entity manager.
		      entityManager = factory.createEntityManager();
		      //Fecha o factory para liberar os recursos utilizado.
		    } finally {
		      factory.close();
		    }
		    return entityManager;
		  }

		  public Contatos salvar(Contatos contato) throws Exception {
		    EntityManager entityManager = getEntityManager();
		    try {
		      // Inicia uma transa��o com o banco de dados.
		      entityManager.getTransaction().begin();
		      System.out.println("Salvando o contato.");
		      // Verifica se o contato ainda n�o est� salvo no banco de dados.
		      if(contato.getId() == null) {
		        //Salva os dados do contato.
		        entityManager.persist(contato);
		      } else {
		        //Atualiza os dados do contato.
		    	  contato = entityManager.merge(contato);
		      }
		      // Finaliza a transa��o.
		      entityManager.getTransaction().commit();
		    } finally {
		      entityManager.close();
		    }
		    return contato;
		  }

		  public void excluir(int id) {
		    EntityManager entityManager = getEntityManager();
		    try {
		      // Inicia uma transa��o com o banco de dados.
		      entityManager.getTransaction().begin();
		      // Consulta o contato na base de dados atrav�s do seu ID.
		      Contatos contato = entityManager.find(Contatos.class, id);
		      System.out.println("Excluindo os dados de: " + contato.getNome());
		      // Remove o contato da base de dados.
		      entityManager.remove(contato);
		      // Finaliza a transa��o.
		      entityManager.getTransaction().commit();
		    } finally {
		      entityManager.close();
		    }
		  }

		  public Contatos consultarPorId(int id) {
		    EntityManager entityManager = getEntityManager();
		    Contatos contato = null;
		    try {
		      //Consulta um contato pelo seu ID.
		      contato = entityManager.find(Contatos.class, id);
		    } finally {
		      entityManager.close();
		    }
		    return contato;
		  }
		}

